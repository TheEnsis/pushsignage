package com.example.pushsignage

import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Environment
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.ImageView
import android.widget.Toast
import androidx.core.net.toUri
import androidx.core.view.isNotEmpty
import com.bumptech.glide.Glide
import com.example.pushsignage.data.SDConfig
import com.example.pushsignage.util.*
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_video_player.*
import java.io.File
import java.util.*
import kotlin.collections.set

class SDVideoPlayerActivity : BaseActivity(), Player.EventListener {

    override fun viewLayoutResource(): View = rootLayout

    private val mediaDataSourceFactory: DataSource.Factory by lazy {
        DefaultDataSourceFactory(
                this,
                Util.getUserAgent(this, BuildConfig.APPLICATION_ID)
        )
    }
    private var width = 0
    private var height = 0

    private lateinit var timerTimeline: CountDownTimer
    private val listTimerContent = mutableListOf<CountDownTimer>()
    private val hashMapVideoPlayer = hashMapOf<String, SimpleExoPlayer>()
    private val basePath = Environment.getExternalStorageDirectory().path + "/htdocs/signage"
    private val contentPath = "/sdmodeconfig.json"

    private var sdConfig = SDConfig()
    private val listFile = mutableListOf<File>()
    private var currentElementIndex = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_player)
        initSystemUI()
        checkPermission({ initInstance() }, 1122)
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1122 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initInstance()
                } else
                    Toast.makeText(
                            this,
                            "Please grant storage permission",
                            Toast.LENGTH_SHORT
                    ).show()
            }
            else -> return
        }
    }

    private fun initSystemUI() {
        hideSystemUI()

        window.decorView.setOnSystemUiVisibilityChangeListener { visibility ->
            // Note that system bars will only be "visible" if none of the
            // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
            if (visibility and View.SYSTEM_UI_FLAG_FULLSCREEN == 0) {
                // The system bars are visible. Make any desired
                // adjustments to your UI, such as showing the action bar or
                // other navigational controls.
                delayFunction(1500) { hideSystemUI() }
                Log.e("decorView", "The system bars are visible")
            } else {
                // The system bars are NOT visible. Make any desired
                // adjustments to your UI, such as hiding the action bar or
                // other navigational controls.
                Log.e("decorView", "The system bars are NOT visible")
            }
        }
    }

    private fun initInstance() {
        readContentFromLocalStorage()
        readConfigFromStorage()

        if (listFile.isNotEmpty()) {
            val size = getDisplaySize()
            width = size.x
            height = size.y
            initCampaign()
        } else {
            toast("no content")
        }
    }

    private fun readContentFromLocalStorage() {
        val sdPath = UtilsInJava.getExternalStoragePath(this, true)
        logHelper(sdPath)
        val listMedia = listOf("mp4", "jpg", "jpeg", "mp3", "mpeg4")
        File("$sdPath/")
                .walkTopDown()
                .filter { it.extension.toLowerCase(Locale.getDefault()) in listMedia }
                .filter { !it.nameWithoutExtension.toLowerCase(Locale.getDefault()).startsWith("._") }
                .sortedBy { it.name }
                .toList()
                .toCollection(listFile)
        logHelper(listFile.joinToString { it.name })
    }

    private fun readConfigFromStorage() {
        if (!File(basePath + contentPath).exists()) return
        val inputString: String =
                File(basePath + contentPath).bufferedReader().use { it.readText() }
        logHelper(inputString)
        try {
            sdConfig = GsonHelper.getGson().fromJson(inputString, SDConfig::class.java)
        } catch (e: Exception) {
            toast("config json invalid")
        }
    }

    private fun initCampaign() {
        if (!isFinishing && !isDestroyed) {
            currentElementIndex = 0
            initPlayer()
        } else {
            finish()
        }
    }

    private fun initPlayer() {
        listTimerContent.forEach { it.cancel() }
        hashMapVideoPlayer.values.forEach { it.release() }

        val currentContent = listFile[currentElementIndex]
        val view = filterPlayerContent(currentContent)
        if (view != null) {
            println("add view pass")
            if (rootLayout.isNotEmpty()) rootLayout.removeAllViews()
            rootLayout.addView(view)
            if (view is ImageView)
                startCountDownContentDuration(currentElementIndex)
        } else {
            toast("no file available in storage")
            setNextIndex()
            initPlayer()
        }
    }

    private fun filterPlayerContent(currentContent: File): View? {
        currentContent.also { content ->
            val listMedia = listOf("mp4", "mp3", "mpeg4")
            val listImage = listOf("jpg", "jpeg")
            if (content.extension in listImage) {
                return createImagePlayer(content)
            } else if (content.extension in listMedia) {
                return createVideoPlayer(content)
            }
        }
        return null
    }

    private fun createImagePlayer(content: File): ImageView {
        val layoutImage = ImageView(this).apply {
            layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
            adjustViewBounds = true
        }
        Glide.with(this)
                .asBitmap()
                .load(content.toUri())
                .dontAnimate()
                .into(layoutImage)
        return layoutImage
    }

    private fun startCountDownContentDuration(index: Int = 0) {
        val timerContent = object : CountDownTimer(sdConfig.imageDuration * 1000L, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                println("Content ${listFile[index].path} duration = " + +millisUntilFinished / 1000)
            }

            override fun onFinish() {
                logHelper("onFinish selectedContent - ${listFile[index].path}")
                setNextIndex()
                initPlayer()
            }
        }
        timerContent.start()
        listTimerContent.add(timerContent)
    }

    private fun createVideoPlayer(currentContent: File): View? {
        if (!currentContent.exists()) return null

        val mediaSource = ProgressiveMediaSource.Factory(mediaDataSourceFactory)
                .createMediaSource(Uri.parse("file:////${currentContent.path}"))

        val exoPlayer = ExoPlayerFactory.newSimpleInstance(this).apply {
            prepare(mediaSource)
            playWhenReady = true
//            if (currentContent.muted) volume = 0f
            addListener(this@SDVideoPlayerActivity)
        }
        hashMapVideoPlayer[currentContent.path] = exoPlayer
        val playerView = CustomPlayerView(this).apply {
            useController = false
            setBackgroundColor(Color.TRANSPARENT)
            resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FIXED_WIDTH
            when (sdConfig.mediaScale) {
                "fill" -> resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FILL
                "cover" -> resizeMode = AspectRatioFrameLayout.RESIZE_MODE_ZOOM
                "contain" -> resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FIXED_WIDTH
                "scale-down" -> resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FIXED_WIDTH
            }
            isFocusable = true
            setShutterBackgroundColor(Color.TRANSPARENT)
            player = exoPlayer
            requestFocus()
        }
        return playerView
    }

    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
        when (playbackState) {
            // track ended play next if there is one
            Player.STATE_BUFFERING -> {

            }
            Player.STATE_ENDED -> {
                setNextIndex()
                initPlayer()
            }
            Player.STATE_READY -> {
                // todo handle next
            }
        }
    }

    private fun setNextIndex() {
        currentElementIndex =
                if (currentElementIndex + 1 < listFile.size) ++currentElementIndex else 0
    }

    override fun onStart() {
        super.onStart()
        if (isNetworkConnectedOrConnecting()) {
            FirebaseDatabase.getInstance().getReference(BuildConfig.PREFIX_TYPE)
                    .child(branchName.toLowerCase())
                    .child(directoryName.toLowerCase())
                    .child("is_playing").setValue(1)
        }
    }

    override fun onDestroy() {
        if (::timerTimeline.isInitialized) timerTimeline.cancel()
        listTimerContent.forEach { it.cancel() }
        hashMapVideoPlayer.values.forEach { it.release() }
        super.onDestroy()
    }

    private fun hideSystemUI() {
        window.decorView.systemUiVisibility = (//View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        or View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        or View.SYSTEM_UI_FLAG_IMMERSIVE)
    }
}