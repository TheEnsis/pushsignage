package com.example.pushsignage.data


import com.google.gson.annotations.SerializedName

data class Content(
        @SerializedName("media_id")
        val mediaId: Int = 0, // 182
        @SerializedName("type")
        val type: String = "", // video
        @SerializedName("path")
        val path: String = "", // assets/1566271147_54620.mp4
        @SerializedName("start_time_offset")
        val startTimeOffset: Int = 0, // 0
        @SerializedName("filestartdate")
        val fileStartDate: String? = "", // 2019-08-20
        @SerializedName("fileenddate")
        val fileEndDate: String? = "", // 2025-12-31
        @SerializedName("startdate")
        val startDate: String? = "",
        @SerializedName("enddate")
        val endDate: String? = "",
        @SerializedName("duration")
        val duration: Int = 0, // 30
        @SerializedName("loop")
        val loop: Boolean = false, // true
        @SerializedName("scale")
        val scale: String = "", // cover
        @SerializedName("autoplay")
        val autoPlay: Boolean = false, // true
        @SerializedName("muted")
        val muted: Boolean = false // true
)