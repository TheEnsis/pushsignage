package com.example.pushsignage.data


import com.google.gson.annotations.SerializedName

data class TimelineElement(
        @SerializedName("id")
        val id: Int = 0, // 44
        @SerializedName("layout_id")
        val layoutId: Int = 0, // 30
        @SerializedName("duration")
        val duration: Int = 0, // 15
        @SerializedName("slots")
        val slots: List<SlotX> = listOf()
)