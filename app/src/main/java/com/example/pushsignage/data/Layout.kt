package com.example.pushsignage.data


import com.google.gson.annotations.SerializedName

data class Layout(
        @SerializedName("layout_id")
        val layoutId: Int = 0, // 30
        @SerializedName("width")
        val width: String? = "", // 1080px
        @SerializedName("height")
        val height: String? = "", // 1920px
        @SerializedName("background_color")
        val backgroundColor: String = "", // #ff0000
        @SerializedName("slots")
        val slots: List<Slot> = listOf()
)