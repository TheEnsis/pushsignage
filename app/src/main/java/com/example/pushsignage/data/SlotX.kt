package com.example.pushsignage.data


import com.google.gson.annotations.SerializedName

data class SlotX(
        @SerializedName("slot_id")
        val slotId: Int = 0, // 53
        @SerializedName("playlist_id")
        val playlistId: Int = 0 // 42
)