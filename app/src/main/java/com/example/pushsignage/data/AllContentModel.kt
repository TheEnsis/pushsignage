package com.example.pushsignage.data


import com.google.gson.annotations.SerializedName

data class AllContentModel(
        @SerializedName("campaign")
        val campaign: List<Campaign> = listOf(),
        @SerializedName("playlists")
        val playList: List<Playlist> = listOf(),
        @SerializedName("layouts")
        val layouts: List<Layout> = listOf()
)