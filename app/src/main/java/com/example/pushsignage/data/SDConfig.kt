package com.example.pushsignage.data

import com.google.gson.annotations.SerializedName

data class SDConfig(
    @SerializedName("image_duration")
    val imageDuration: Int = 30,
    @SerializedName("media_scale")
    val mediaScale: String = "fill"
)