package com.example.pushsignage.data


import com.google.gson.annotations.SerializedName

data class Slot(
        @SerializedName("slot_id")
        val slotId: Int = 0, // 53
        @SerializedName("x")
        val x: String = "", // 0px
        @SerializedName("y")
        val y: String = "", // 0px
        @SerializedName("width")
        val width: String = "", // 1080px
        @SerializedName("height")
        val height: String = "", // 1920px
        @SerializedName("z-index")
        val zIndex: Int = 0 // 0
)