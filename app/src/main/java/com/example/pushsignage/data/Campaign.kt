package com.example.pushsignage.data


import com.google.gson.annotations.SerializedName

data class Campaign(
        @SerializedName("timeline_id")
        val timelineId: Int = 0, // 28
        @SerializedName("timeline_name")
        val timelineName: String = "", // CodexTimeline01
        @SerializedName("startdate")
        val startDate: String = "", // 2019-08-01
        @SerializedName("enddate")
        val endDate: String = "", // 2019-08-30
        @SerializedName("starttime")
        val startTime: String = "", // 00:00:00
        @SerializedName("endtime")
        val endTime: String = "", // 23:59:00
        @SerializedName("timeline_element")
        val timelineElement: List<TimelineElement> = listOf()
)