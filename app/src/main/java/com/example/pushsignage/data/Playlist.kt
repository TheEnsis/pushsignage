package com.example.pushsignage.data


import com.google.gson.annotations.SerializedName

data class Playlist(
        @SerializedName("playlist_id")
        val playlistId: Int = 0, // 43
        @SerializedName("contents")
        val contents: List<Content> = listOf()
)