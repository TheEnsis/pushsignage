package com.example.pushsignage

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Environment
import android.os.SystemClock
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.example.pushsignage.service.AwesomeBackgroundService
import com.example.pushsignage.service.BackgroundAlarmReceiver
import com.example.pushsignage.util.SharePrefManager
import com.example.pushsignage.util.getNowTime
import com.example.pushsignage.util.logHelper
import com.example.pushsignage.util.toast
import com.google.firebase.FirebaseApp
import com.google.firebase.database.FirebaseDatabase
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import net.danlew.android.joda.JodaTimeAndroid
import java.io.File
import java.io.IOException
import kotlin.system.exitProcess


class MainApplication : MultiDexApplication(), LifecycleObserver {

    companion object {
        const val APP_CREATE = 0
        const val APP_START = 1
        const val APP_STOP = 2
        const val APP_DESTROY = 3
    }

    private var appState = 0
    var alarmMgr: AlarmManager? = null
    private lateinit var alarmIntent: PendingIntent

    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(this)
        SharePrefManager.init(this)
        Logger.addLogAdapter(AndroidLogAdapter())
        JodaTimeAndroid.init(this)
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)

        alarmMgr = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmIntent = Intent(this, BackgroundAlarmReceiver::class.java).let { intent ->
            PendingIntent.getBroadcast(this, 0, intent, 0)
        }

        startIntervalAlarm()

        Thread.setDefaultUncaughtExceptionHandler { thread, e ->
            handleUncaughtException(thread, e)
        }

        if (SharePrefManager.getBooleanFromPref(BaseActivity.IS_SETUP)) {
            val branchName = SharePrefManager.getStringFromPref(BaseActivity.FTP_BRANCH)
            val directoryName = SharePrefManager.getStringFromPref(BaseActivity.FTP_DIRECTORY)
            val database = FirebaseDatabase.getInstance()
            database.getReference(BuildConfig.PREFIX_TYPE).child(branchName.toLowerCase())
                    .child(directoryName.toLowerCase()).child("is_online").setValue(1)
            database.getReference(BuildConfig.PREFIX_TYPE).child(branchName.toLowerCase())
                    .child(directoryName.toLowerCase()).child("is_online").onDisconnect().setValue(0)
        }
    }

    fun startIntervalAlarm() {
        // Hopefully your alarm will have a lower frequency than this!
        alarmMgr?.setInexactRepeating(
                AlarmManager.ELAPSED_REALTIME_WAKEUP,
                (SystemClock.elapsedRealtime() + (1 * 60 * 1000)),//AlarmManager.INTERVAL_HALF_HOUR,
                (1 * 60 * 1000).toLong(),//AlarmManager.INTERVAL_HALF_HOUR,
                alarmIntent
        )
    }

    fun stopIntervalAlarm() {
        alarmMgr?.cancel(alarmIntent)
    }

    private fun updateAppStatus() {
        logHelper("updateAppStatus = $appState")
        if (SharePrefManager.getBooleanFromPref("autoStart", false)) {
            toast("App Restart")
            val serviceIntent = Intent(this, AwesomeBackgroundService::class.java)
            serviceIntent.putExtra(AwesomeBackgroundService.APPLICATION_STATUS, appState)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                ContextCompat.startForegroundService(this, serviceIntent)
            } else {
                startService(serviceIntent)
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun appOnCreated() {
        appState = APP_CREATE
        updateAppStatus()
        Log.e("Awww", "App in created")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun appOnForegrounded() {
        appState = APP_START
        updateAppStatus()
        Log.e("Yeeey", "App in foreground")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun appOnBackgrounded() {
        appState = APP_STOP
        updateAppStatus()
        Log.e("Awww", "App in background")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun appOnDestroyed() {
        appState = APP_DESTROY
        stopIntervalAlarm()
        updateAppStatus()
        Log.e("Awww", "App in destroy")
    }


    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
//        ACRA.init(this)
    }

    private fun handleUncaughtException(thread: Thread, e: Throwable) {
//        logToCrashlytics(e) //add our crashlytics logging and keys here

        var arr = e.stackTrace
        var report = e.toString() + "\n\n"
        report += "--------- Stack trace ---------\n\n"
        for (i in arr.indices) {
            report += "    " + arr[i].toString() + "\n"
        }
        report += "-------------------------------\n\n"

        // If the exception was thrown in a background thread inside
        // AsyncTask, then the actual exception can be found with getCause

        // If the exception was thrown in a background thread inside
        // AsyncTask, then the actual exception can be found with getCause
        report += "--------- Cause ---------\n\n"
        val cause: Throwable? = e.cause
        if (cause != null) {
            report += cause.toString() + "\n\n"
            arr = cause.stackTrace
            for (i in arr.indices) {
                report += "    " + arr[i].toString() + "\n"
            }
        }
        report += "-------------------------------\n\n"
        println(report)
        try {
            val pathFolderLog = Environment.getExternalStorageDirectory().path + "/htdocs" + "/log"
            if (!File(pathFolderLog).exists()) File(pathFolderLog).mkdir()
            val savePath = "$pathFolderLog/stack_${getNowTime()}.trace"
            val saveFile = File(savePath)
            if (!saveFile.exists()) saveFile.createNewFile()
            saveFile.writeText(report)
//            val i = Intent(Intent.ACTION_SEND);
//            i.setType("message/rfc822");
//            i.putExtra(Intent.EXTRA_EMAIL, arrayListOf<String>("theesniz@gmail.com"));
//            i.putExtra(Intent.EXTRA_SUBJECT, "crash report azar");
//            val body = "Mail this to : " + "\n" + trace + "\n";
//            i.putExtra(Intent.EXTRA_TEXT, body);
//            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (ioe: IOException) { // ...
        } finally {
            if (SharePrefManager.getBooleanFromPref("autoStart", false)) {
                val intent = Intent(applicationContext, SetupActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED)
                startActivity(intent)
            }
            android.os.Process.killProcess(android.os.Process.myPid())
            exitProcess(10)
        }
    }

    private fun logToCrashlytics(e: Throwable) {
//        val branchName = SharePrefManager.getStringFromPref(BaseActivity.FTP_BRANCH)
//        val directoryName = SharePrefManager.getStringFromPref(BaseActivity.FTP_DIRECTORY)
//        val crashlytics = FirebaseCrashlytics.getInstance()
//
//        crashlytics.setCustomKey("branch_name", branchName)
//        crashlytics.setCustomKey("directory_name", directoryName)
//        FirebaseCrashlytics.getInstance().recordException(e)
    }
}