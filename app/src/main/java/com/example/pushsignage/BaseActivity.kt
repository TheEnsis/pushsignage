package com.example.pushsignage

import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Point
import android.net.Uri
import android.os.Environment
import android.view.TextureView
import android.view.View
import android.view.View.MeasureSpec
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.children
import androidx.core.view.drawToBitmap
import androidx.core.view.get
import com.example.pushsignage.util.*
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.firebase.database.*
import java.io.File


abstract class BaseActivity : AppCompatActivity() {

    companion object {
        internal const val IS_SETUP = "is_setup"

        internal const val FTP_HOST = "ftp_host"
        internal const val FTP_USER = "ftp_user"
        internal const val FTP_PASS = "ftp_pass"
        internal const val FTP_BRANCH = "ftp_branch"
        internal const val FTP_DIRECTORY = "ftp_directory"

        internal const val NEED_RELOAD_PLAYER = "needs_reload_player"
        internal const val NEED_UPDATE_PLAYER = "needs_update_player"
        internal const val NEED_DOWNLOAD_CONTENT = "needs_download_content"
        internal const val NEED_CLEAN_TRASH = "needs_clean_trash"
        internal const val PLAY_MODE = "play_mode"
        internal val LIST_KEY =
                listOf(NEED_RELOAD_PLAYER, NEED_UPDATE_PLAYER, NEED_DOWNLOAD_CONTENT, NEED_CLEAN_TRASH, PLAY_MODE)
        internal const val NEED_CAPTURE_SCREEN = "needs_capture_screen"
        internal const val PLAYER_DIR_NAME = "player"
        internal const val ASSET_DIR_NAME = "assets"
        internal const val CONTENT_DIR_NAME = "content"
    }

    internal val name = SharePrefManager.getStringFromPref(FTP_HOST)
    internal val user = SharePrefManager.getStringFromPref(FTP_USER)
    internal val pass = SharePrefManager.getStringFromPref(FTP_PASS)
    internal val branchName = SharePrefManager.getStringFromPref(FTP_BRANCH)
    internal val directoryName = SharePrefManager.getStringFromPref(FTP_DIRECTORY)
//    val storageRef = FirebaseStorage.getInstance().reference.child("${branchName.toLowerCase()}/${directoryName.toLowerCase()}")

    abstract fun viewLayoutResource(): View

//    private var captureRef: DatabaseReference = FirebaseDatabase.getInstance()
//            .getReference("${BuildConfig.PREFIX_TYPE}/${branchName.toLowerCase()}/${directoryName.toLowerCase()}")
//            .child(NEED_CAPTURE_SCREEN)

    private var isActivityVisible = false
    internal var rootView: RelativeLayout? = null

    override fun onResume() {
        super.onResume()
        rootView = viewLayoutResource() as? RelativeLayout
        isActivityVisible = true
//        logHelper("$localClassName isActivityVisible = $isActivityVisible")
//        if (rootView != null) captureRef.addValueEventListener(captureListener)
    }

    override fun onPause() {
        super.onPause()
        isActivityVisible = false
//        logHelper("$localClassName isActivityVisible = $isActivityVisible")
//        captureRef.removeEventListener(captureListener)
    }

//    private val captureListener = object : ValueEventListener {
//        override fun onCancelled(databaseError: DatabaseError) {
//            logHelper(databaseError.message)
//        }
//
//        override fun onDataChange(dataSnapshot: DataSnapshot) {
//            val value = dataSnapshot.getValue(Int::class.java)
////            logHelper("onDataSnapshotChanged - " + dataSnapshot.key + " : " + value.toString())
//
//            if (value == null) {
//                dataSnapshot.ref.setValue(0)
//                return
//            }
//            // capture screen
//            if (value == 1) {
//                if (rootView != null) {
//                    // TODO get bitmap programmatically by check class if vdo or else
//                    val newImage: Bitmap
//                    if (localClassName == "VideoPlayerActivity") {
//                        // case vdo have to check type vdo and get by loop view
//                        val newLayout = RelativeLayout(this@BaseActivity).apply {
//                            layoutParams = RelativeLayout.LayoutParams(
//                                    ViewGroup.LayoutParams.MATCH_PARENT,
//                                    ViewGroup.LayoutParams.MATCH_PARENT
//                            )
//                            setBackgroundColor(Color.BLACK)
//                        }
//                        for (layout in rootView!!.children) {
//                            if (layout !is FrameLayout) return
//                            if (layout[0] is ImageView) {
//                                newLayout.addView(layout, layout.layoutParams)
//                            } else if (layout[0] is CustomPlayerView) {
//                                val newImageView = ImageView(this@BaseActivity).apply {
//                                    layoutParams = layout.layoutParams
//                                    requestLayout()
//                                    val player = (layout[0] as CustomPlayerView)
//                                    scaleType = when (player.resizeMode) {
//                                        AspectRatioFrameLayout.RESIZE_MODE_FILL -> ImageView.ScaleType.FIT_XY
//                                        AspectRatioFrameLayout.RESIZE_MODE_ZOOM -> ImageView.ScaleType.CENTER_CROP
//                                        AspectRatioFrameLayout.RESIZE_MODE_FIXED_WIDTH -> ImageView.ScaleType.FIT_CENTER
//                                        else -> ImageView.ScaleType.FIT_CENTER
//                                    }
//                                    setImageBitmap((player.videoSurfaceView as TextureView).bitmap)
//                                }
//                                newLayout.addView(newImageView, layout.layoutParams)
//                            }
//                        }
//                        newImage = newLayout.loadBitmapFromView()
//                        newLayout.removeAllViews()
//                    } else {
//                        // other class has once view
//                        newImage = rootView!!.drawToBitmap()
//                    }
//
//                    val bitmapFile = bitmapToFile(newImage)
//                    dataSnapshot.ref.setValue(0)
////                    // Create file metadata including the content type
//                    val metadata = StorageMetadata.Builder()
//                            .setContentType("image/jpg")
//                            .build()
//
//                    val file = Uri.fromFile(File(bitmapFile.path!!))
//                    val riversRef = storageRef.child(file.lastPathSegment!!)
//                    val uploadTask = riversRef.putFile(file, metadata)
//
//                    // Register observers to listen for when the download is done or if it fails
//                    uploadTask.addOnFailureListener {
//                        // Handle unsuccessful uploads
//                        it.printStackTrace()
//                    }.addOnSuccessListener {
//                        // taskSnapshot.metadata contains file metadata such as size, content-type, etc.
//                        // ...
//                        dataSnapshot.ref.setValue(0)
//                        toast("capture screen")
//                    }
//                }
//            }
//        }
//    }

//    fun getScreenViewBitmap(v: View): Bitmap {
//        v.isDrawingCacheEnabled = true
//
//        v.measure(
//                MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
//                MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)
//        )
//        v.layout(0, 0, v.measuredWidth, v.measuredHeight)
//
//        v.buildDrawingCache(true)
//        val b = Bitmap.createBitmap(v.drawingCache)
//        v.isDrawingCacheEnabled = false // clear drawing cache
//
//        return b
//    }

    internal fun getDisplaySize(): Point {
        val display = windowManager.defaultDisplay
        val size = Point()
        display.getRealSize(size)
        return size
    }

    internal val isExternalStorageReadOnly: Boolean
        get() {
            val extStorageState = Environment.getExternalStorageState()
            return Environment.MEDIA_MOUNTED_READ_ONLY == extStorageState
        }
    internal val isExternalStorageAvailable: Boolean
        get() {
            val extStorageState = Environment.getExternalStorageState()
            return Environment.MEDIA_MOUNTED == extStorageState
        }
}