package com.example.pushsignage.util

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit

@SuppressLint("StaticFieldLeak")
object SharePrefManager {

    private const val prefKey = "APP_SHARE_PREFERENCE"
    private lateinit var sharedPref: SharedPreferences

    fun init(context: Context) {
        sharedPref = context.getSharedPreferences(prefKey, Context.MODE_PRIVATE)
    }

    fun saveStringToPref(key: String, value: String) {
        sharedPref.edit { putString(key, value) }
    }

    fun getStringFromPref(key: String): String = sharedPref.getString(key, "") ?: ""

    fun getStringFromPref(key: String, default: String): String = sharedPref.getString(key, default) ?: default

    fun saveLongToPref(key: String, value: Long) {
        sharedPref.edit { putLong(key, value) }
    }

    fun getLongFromPref(key: String): Long = sharedPref.getLong(key, 0)

    fun saveIntToPref(key: String, value: Int) {
        sharedPref.edit { putInt(key, value) }
    }

    fun getIntFromPref(key: String): Int = sharedPref.getInt(key, 0)

    fun commitBooleanToPref(key: String, status: Boolean) {
        sharedPref.edit(commit = true) { putBoolean(key, status) }
    }

    fun saveBooleanToPref(key: String, status: Boolean) {
        sharedPref.edit { putBoolean(key, status) }
    }

    fun getBooleanFromPref(key: String): Boolean = sharedPref.getBoolean(key, false)

    fun getBooleanFromPref(key: String, default: Boolean): Boolean = sharedPref.getBoolean(key, default)

}