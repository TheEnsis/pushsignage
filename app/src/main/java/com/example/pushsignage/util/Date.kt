package com.example.pushsignage.util

import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import org.joda.time.DateTime
import org.joda.time.Instant
import org.joda.time.format.DateTimeFormat


private val cy = Calendar.getInstance().get(Calendar.YEAR)
private val localeThai = Locale("th", "th")
private val localEng = Locale.US

fun getLastUpdateTime(): String {
    val c = Calendar.getInstance()
    val dateFormat = SimpleDateFormat("HH.mm", localEng)
    return dateFormat.format(c.time)
}

fun getNowDate(): String {
    val c = Calendar.getInstance()
    val dateFormat = SimpleDateFormat("yyyy-MM-dd", localEng)
    return dateFormat.format(c.time)
}

fun getNowTime(): String {
    val c = Calendar.getInstance()
    val dateFormat = SimpleDateFormat("yyyy-MM-dd_HH-mm-ss", localEng)
    return dateFormat.format(c.time)
}

fun getDateTimeInstantFormat(date: String, time: String): Instant {
    val dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
//    val parsedDateTimeUsingFormatter = DateTime.parse(dateString, dateTimeFormatter)
    return Instant.parse("$date $time", dateTimeFormatter)
}

fun getDateInstantFormat(date: String): Instant {
    val dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd")
//    val parsedDateTimeUsingFormatter = DateTime.parse(dateString, dateTimeFormatter)
    return Instant.parse(date, dateTimeFormatter)
}

fun getTimeInstantFormat(time: String): Instant {
//    val dateTimeFormatter = DateTimeFormat.forPattern("HH:mm:ss")
//    val parsedDateTimeUsingFormatter = DateTime.parse(dateString, dateTimeFormatter)
    return getDateTimeInstantFormat(getNowDate(), time)
}

fun getTimeFromTagUsed(dateString: String): String {
    val fmt = SimpleDateFormat("dd/MM/yyyy HH:mm:ss", localEng)
    val date = fmt.parse(dateString)
    val fmtOut = SimpleDateFormat("HH:mm:ss", localEng)
    return fmtOut.format(date)
}

fun changeFormatTimeNews(dateString: String): String {
    val fmt = SimpleDateFormat("HH:mm:ss", localEng)
    val date = fmt.parse(dateString)
    val fmtOut = SimpleDateFormat("HH.mm", localEng)
    return fmtOut.format(date)
}
