package com.example.pushsignage.util

import android.content.Context
import android.util.Log
import com.google.gson.reflect.TypeToken
import org.apache.commons.net.ftp.FTP
import org.apache.commons.net.ftp.FTPClient
import org.apache.commons.net.ftp.FTPReply
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.InetAddress
import java.net.SocketException
import java.net.UnknownHostException


const val TAG = "FTPManager"

var mapFilePlayer = hashMapOf<String, Long>()
var mapFileAssets = hashMapOf<String, Long>()
var mapFileName = hashMapOf<String, String>()

/**
 * @param ip
 * @param userName
 * @param pass
 */
fun Context.connectingWithFTP(ip: String, userName: String, pass: String): FTPClient? {
    val status: Boolean
    val type = object : TypeToken<HashMap<String, Long>>() {}.type
    val stringHashMapPlayer = SharePrefManager.getStringFromPref("mapTimePlayer")
    if (stringHashMapPlayer.isNotEmpty()) mapFilePlayer = GsonHelper.getGson().fromJson(stringHashMapPlayer, type)
    val stringHashMapAssets = SharePrefManager.getStringFromPref("mapTimeAssets")
    if (stringHashMapAssets.isNotEmpty()) mapFileAssets = GsonHelper.getGson().fromJson(stringHashMapAssets, type)

    try {
        val mFtpClient = FTPClient()
        mFtpClient.connectTimeout = 10 * 1000
        mFtpClient.connect(InetAddress.getByName(ip))
        status = mFtpClient.login(userName, pass)
        Log.e("isFTPConnected", status.toString())
        if (FTPReply.isPositiveCompletion(mFtpClient.replyCode)) {
            mFtpClient.setFileType(FTP.ASCII_FILE_TYPE)
            mFtpClient.enterLocalPassiveMode()
        }
        return mFtpClient
    } catch (e: SocketException) {
        e.printStackTrace()
        delayFunction(0) { toast(e.localizedMessage) }
        return null
    } catch (e: UnknownHostException) {
        e.printStackTrace()
        delayFunction(0) { toast(e.localizedMessage) }
        return null
    } catch (e: IOException) {
        e.printStackTrace()
        delayFunction(0) { toast(e.localizedMessage) }
        return null
    }
}

/**
 * Download a whole directory from a FTP server.
 * @param ftpClient an instance of org.apache.commons.net.ftp.FTPClient class.
 * @param parentDir Path of the parent directory of the current directory being
 * downloaded.
 * @param currentDir Path of the current directory being downloaded.
 * @param saveDir path of directory where the whole remote directory will be
 * downloaded and saved.
 * @param nameDirToDelete path of directory to remove from saveDir.
 * @throws IOException if any network or IO error occurred.
 */
@Throws(IOException::class)
private fun Context.downloadDirectory(ftpClient: FTPClient, parentDir: String, currentDir: String, saveDir: String, nameDirToDelete: String) {
    var dirToList = parentDir
    if (currentDir != "") {
        dirToList += "/$currentDir"
    }
    logHelper("Download Directory Files $dirToList")
    try {
        val subFiles = ftpClient.listFiles(dirToList)
        if (subFiles != null && subFiles.isNotEmpty()) {
            Log.e(TAG, "subFiles $dirToList = " + subFiles.size.toString())
            for (aFile in subFiles) {
                val currentFileName = aFile.name
                Log.e(TAG, "currentFileName : $currentFileName")
                if (currentFileName == "." || currentFileName == "..") {
                    // skip parent directory and the directory itself
                    Log.e(TAG, "skipFileName : $currentFileName")
                    continue
                }
                var filePath = ("$parentDir/$currentDir/$currentFileName")
                if (currentDir == "") {
                    filePath = "$parentDir/$currentFileName"
                }

                var newDirPath = (saveDir + parentDir + File.separator + currentDir + File.separator + currentFileName)
                if (currentDir == "") {
                    newDirPath = (saveDir + parentDir + File.separator + currentFileName)
                }
                newDirPath = newDirPath.replace(nameDirToDelete, "")

                if (aFile.isDirectory) {
                    // create the directory in saveDir
                    val newDir = File(newDirPath)
                    val created = newDir.mkdirs()
                    if (created) {
                        println("Created the directory: $newDirPath")
                    } else {
                        println("Could not create the directory: $newDirPath")
                    }

                    // download the sub directory
                    downloadDirectory(ftpClient, dirToList, currentFileName, saveDir, nameDirToDelete)
                } else {
                    /** check file in local **/
                    if (File(newDirPath).exists() && mapFilePlayer[aFile.name] == null) {
                        mapFilePlayer[aFile.name] = File(newDirPath).lastModified()
                        println("Found file exist in storage : $filePath}")
                    }

                    /** down load new file **/
                    if (mapFilePlayer[aFile.name] == null || (mapFilePlayer[aFile.name] != aFile.timestamp.timeInMillis || !File(newDirPath).exists())) {
                        val success = downloadSingleFile(ftpClient, filePath, newDirPath)
                        if (success) {
                            println("download new file success : $filePath}")
                            mapFilePlayer[aFile.name] = aFile.timestamp.timeInMillis
                            File(newDirPath).setLastModified(aFile.timestamp.timeInMillis)
                        } else {
                            println("Could not download the file: $filePath")
                        }
                    } else {
                        println("Not download the file Duplicate : $filePath")
                    }
                }
            }
        }
    } catch (ex: Exception) {
        ex.printStackTrace()
        delayFunction(0) { toast(ex.localizedMessage) }
    }
}

/**
 * Download a single file from the FTP server
 * @param ftpClient an instance of org.apache.commons.net.ftp.FTPClient class.
 * @param remoteFilePath path of the file on the server
 * @param savePath path of directory where the file will be stored
 * @return true if the file was downloaded successfully, false otherwise
 * @throws IOException if any network or IO error occurred.
 */
@Throws(IOException::class)
fun Context.downloadSingleFile(ftpClient: FTPClient, remoteFilePath: String, savePath: String): Boolean {
    val downloadFile = File(savePath)

    val parentDir = downloadFile.parentFile
    if (!parentDir.exists()) {
        parentDir.mkdir()
    }
    val outputStream = BufferedOutputStream(FileOutputStream(downloadFile))
    try {
        ftpClient.setFileType(FTP.BINARY_FILE_TYPE)
        return ftpClient.retrieveFile(remoteFilePath, outputStream)
    } catch (ex: IOException) {
        delayFunction(0) { toast(ex.localizedMessage) }
        throw ex
    } finally {
        outputStream.close()
    }
}

/**
 * @param ftpClient  FTP client object
 * @param pathToSave   local file path where you want to save after download
 * @return status of downloaded file
 */
fun Context.downloadMultipleFiles(ftpClient: FTPClient, pathToSave: String, pathFile: String, nameDirToDelete: String, nameToLoad: List<String> = listOf()): Boolean {
    if (!File(pathToSave).exists())
        File(pathToSave).mkdir()
    logHelper("Download Multiple Files $pathFile")
    try {
        val mFileArray = ftpClient.listFiles(pathFile)
        if (mFileArray.isNotEmpty()) {
            for (aFile in mFileArray) {
                val currentFileName = aFile.name
                Log.e(TAG, "currentFileName : $currentFileName")
                if (currentFileName == "." || currentFileName == "..") {
                    // skip parent directory and the directory itself
                    Log.e(TAG, "skipFileName : $currentFileName")
                    continue
                }
                if (nameToLoad.isNotEmpty() && !nameToLoad.contains(currentFileName)) {
                    Log.e(TAG, " $currentFileName is not content in list to load")
                    continue
                }
                if (aFile.isDirectory) {
                    downloadDirectory(ftpClient, "/$pathFile/${aFile.name}", "", pathToSave, "/$nameDirToDelete")
                } else if (aFile.isFile) {
                    /** check file in local **/
                    if (File(pathToSave + "/" + aFile.name).exists() && mapFileAssets[aFile.name] == null) {
                        mapFileAssets[aFile.name] = File(pathToSave + "/" + aFile.name).lastModified()
                        println("Not download the file already exist : ${aFile.name} / ${File(pathToSave + "/" + aFile.name).lastModified()}")
                    }
                    /** download new file **/
                    if (mapFileAssets[aFile.name] == null || (mapFileAssets[aFile.name] != aFile.timestamp.timeInMillis || !File(pathToSave + "/" + aFile.name).exists())) {
                        val downloadFile = File(pathToSave + "/" + aFile.name)
                        val outputStream = BufferedOutputStream(FileOutputStream(downloadFile))
                        var retrieve = false
                        try {
                            ftpClient.setFileType(FTP.BINARY_FILE_TYPE)
                            retrieve = ftpClient.retrieveFile(pathFile + "/" + aFile.name, outputStream)
                        } catch (e: Exception) {
                            e.printStackTrace()
                            toast(e.localizedMessage)
                        } finally {
                            outputStream.close()
                            println("download new file success : ${aFile.name} / ${aFile.timestamp.timeInMillis} -> $retrieve")
                            mapFileAssets[aFile.name] = aFile.timestamp.timeInMillis
                            File(pathToSave + "/" + aFile.name).setLastModified(aFile.timestamp.timeInMillis)
                            print("new last modify = ${File(pathToSave + "/" + aFile.name).lastModified()}")
                        }
                    } else {
                        println("Not download the file Duplicate : ${aFile.name}")
                    }
                }
            }
        }
        return true
    } catch (ex: Exception) {
        ex.printStackTrace()
        delayFunction(0) { toast(ex.localizedMessage) }
    } finally {
//        closeFTP(ftpClient)
    }
    return false
}

@Throws(IOException::class)
fun Context.checkFileInDirectory(ftpClient: FTPClient, parentDir: String, currentDir: String, saveDir: String, nameDirToDelete: String) {
    var dirToList = parentDir
    if (currentDir != "") {
        dirToList += "/$currentDir"
    }
    logHelper("Check Directory Files $dirToList")
    try {


        val subFiles = ftpClient.listFiles(dirToList)
        if (subFiles != null && subFiles.isNotEmpty()) {
            Log.e(TAG, "subFiles $dirToList = " + subFiles.size.toString())
            for (aFile in subFiles) {
                val currentFileName = aFile.name
                Log.e(TAG, "currentFileName : $currentFileName")
                if (currentFileName == "." || currentFileName == "..") {
                    // skip parent directory and the directory itself
                    Log.e(TAG, "skipFileName : $currentFileName")
                    continue
                }
                var filePath = ("$parentDir/$currentDir/$currentFileName")
                if (currentDir == "") {
                    filePath = "$parentDir/$currentFileName"
                }

                if (aFile.isDirectory) {
                    // check the sub directory
                    checkFileInDirectory(ftpClient, dirToList, currentFileName, saveDir, nameDirToDelete)
                } else {
                    // check the file
                    println("Check the file success : $filePath")
                    mapFileName[aFile.name] = filePath.replace(nameDirToDelete, "")
                }
            }
        }
    } catch (ex: Exception) {
        ex.printStackTrace()
        delayFunction(0) { toast(ex.localizedMessage) }
    }
}

fun Context.checkMultipleFiles(ftpClient: FTPClient, pathToSave: String, pathFile: String, nameDirToDelete: String, nameDirToReplace: String = "", nameToLoad: List<String> = listOf()): Boolean {
    if (!File(pathToSave).exists())
        File(pathToSave).mkdir()
    logHelper("Check Multiple Files $pathFile")
    try {
        val mFileArray = ftpClient.listFiles(pathFile)
        if (mFileArray.isNotEmpty()) {
            for (aFile in mFileArray) {
                val currentFileName = aFile.name
                Log.e(TAG, "currentFileName : $currentFileName")
                if (currentFileName == "." || currentFileName == "..") {
                    // skip parent directory and the directory itself
                    Log.e(TAG, "skipFileName : $currentFileName")
                    continue
                }
                if (nameToLoad.isNotEmpty() && !nameToLoad.contains(currentFileName)) {
                    Log.e(TAG, " $currentFileName is not content in list to load")
                    continue
                }
                if (aFile.isDirectory) {
                    checkFileInDirectory(ftpClient, "/$pathFile/${aFile.name}", "", pathToSave, "/$nameDirToDelete")
                } else if (aFile.isFile) {
                    println("Check the file: ${aFile.name} -> true")
                    mapFileName[aFile.name] = (pathFile + "/" + aFile.name).replace(nameDirToDelete, nameDirToReplace)
                }
            }
        }
        return true
    } catch (ex: Exception) {
        ex.printStackTrace()
        delayFunction(0) { toast(ex.localizedMessage) }
    } finally {
//        closeFTP(ftpClient)
//        compareDifFile()
    }
    return false
}

fun compareDifFile(pathToSave: String) {
    logHelper("Start Compare File Host")
    val listFileInHost = mutableListOf<String>()
    listFileInHost.addAll(mapFileName.values)
    listFileInHost.sorted().forEach {
        println(it)
    }
    logHelper("Start Compare File Local")
    val listFileInLocal = File("$pathToSave/").walk().filter { it.absolutePath.contains(".") || it.isFile }
            .map { it.absolutePath.replace(pathToSave, "") }.toMutableList()
    listFileInLocal.forEach {
        println(it)
    }
    logHelper("File Not In Host")
    listFileInLocal.subtract(listFileInHost).forEach {
        println(it)
        val isDelete = File(pathToSave + it).delete()
        println("$it - deleted : $isDelete")
    }
    logHelper("Delete Empty Folder")
    val listFolderEmptyInLocal = File("$pathToSave/").walk().filter { !it.absolutePath.contains(".") && it.isDirectory }
            .map { it.absolutePath.replace(pathToSave, "") }.toMutableList()
    listFolderEmptyInLocal.forEach {
        val isFileEmpty = File(pathToSave + it).listFiles().isEmpty()
        println("$it - empty : $isFileEmpty")
        if (isFileEmpty) {
            val isDelete = File(pathToSave + it).delete()
            println("$it - folder deleted : $isDelete")
        }
    }
}

fun closeFTP(ftpClient: FTPClient) {
    ftpClient.logout()
    ftpClient.disconnect()

    SharePrefManager.saveStringToPref("mapTimePlayer", GsonHelper.getGson().toJson(mapFilePlayer))
    SharePrefManager.saveStringToPref("mapTimeAssets", GsonHelper.getGson().toJson(mapFileAssets))
}