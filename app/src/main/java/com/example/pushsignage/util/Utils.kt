package com.example.pushsignage.util

import android.Manifest
import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.Build
import android.view.View
import android.view.ViewTreeObserver
import androidx.core.content.ContextCompat
import com.example.pushsignage.BuildConfig
import com.example.pushsignage.SetupActivity
import com.orhanobut.logger.Logger
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.util.*

fun delayFunction(delay: Long, unit: () -> Unit) {
    GlobalScope.launch(Dispatchers.Main) {
        kotlinx.coroutines.delay(delay)
        unit()
    }
}

fun delayIOFunction(delay: Long, unit: () -> Unit) {
    GlobalScope.launch(Dispatchers.IO) {
        kotlinx.coroutines.delay(delay)
        unit()
    }
}

fun logHelper(message: String?) {
    if (BuildConfig.DEBUG) {
        message?.let { Logger.e(it) }
    }
}

fun String.fromPxToDouble(): Double {
    return this.replace("px", "").toDouble()
}

fun String.fromPxToInt(): Int {
    return this.replace("px", "").toInt()
}

fun View.loadBitmapFromView(): Bitmap {
    measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
    val b = Bitmap.createBitmap(measuredWidth, measuredHeight, Bitmap.Config.ARGB_8888)
    val c = Canvas(b)
    layout(0, 0, width, height)
    draw(c)
    return b
}

fun Context.bitmapToFile(bitmap: Bitmap): Uri {
    // Get the context wrapper
    val wrapper = ContextWrapper(applicationContext)

    // Initialize a new file instance to save bitmap object
    var file = wrapper.getDir("Images", Context.MODE_PRIVATE)
    file = File(file, "image_${getNowTime()}.jpg")

    try {
        // Compress the bitmap and save in jpg format
        val stream: OutputStream = FileOutputStream(file)
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
        stream.flush()
        stream.close()
    } catch (e: IOException) {
        e.printStackTrace()
    }

    // Return the saved bitmap uri
    return Uri.parse(file.absolutePath)
}

inline fun View.waitForLayout(crossinline f: () -> Unit) = with(viewTreeObserver) {
    addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            removeOnGlobalLayoutListener(this)
            f()
        }
    })
}

fun Context.isNetworkConnectedOrConnecting(): Boolean {
    val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
    val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
    return isConnected
}

fun Context.restartApp() {
    val context = applicationContext
    val mStartActivity = Intent(context, SetupActivity::class.java)
    val mPendingIntentId = (Date().time * .1).toInt()
    val mPendingIntent = PendingIntent.getActivity(context, mPendingIntentId, mStartActivity, PendingIntent.FLAG_UPDATE_CURRENT)
    val mgr = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 5000, mPendingIntent)
}


fun Activity.checkPermission(unit: () -> Unit, code: Int) {
    val permissionCheck =
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
        unit()
    } else {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), code)
        }
    }
}
