package com.example.pushsignage.util

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import java.text.DateFormat

/**
 * Created by Nut on 10/11/17.
 */
object GsonHelper {
    fun getGson(): Gson = GsonBuilder()
            .setDateFormat(DateFormat.LONG)
            .enableComplexMapKeySerialization()
            .serializeNulls()
            .disableHtmlEscaping()
            .setLenient()
            .setPrettyPrinting()
            .create()
}