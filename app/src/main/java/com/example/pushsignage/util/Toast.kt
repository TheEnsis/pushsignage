package com.example.pushsignage.util

import android.annotation.SuppressLint
import android.content.Context
import android.widget.Toast
import com.example.pushsignage.BuildConfig

@SuppressLint("ShowToast")
fun Context.toast(string: String) {
    var toast: Toast? = null
    try {
        toast!!.view.isShown
        toast.setText(string)

    } catch (e: Exception) {
        toast = Toast.makeText(this, string, Toast.LENGTH_SHORT)
    }
    if (BuildConfig.DEBUG)
        toast!!.show()
}