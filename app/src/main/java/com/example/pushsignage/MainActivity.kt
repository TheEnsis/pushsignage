package com.example.pushsignage

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.*
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import com.example.pushsignage.service.AwesomeBackgroundService
import com.example.pushsignage.service.ServiceManager
import com.example.pushsignage.util.*
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.File

class MainActivity : BaseActivity() {

    override fun viewLayoutResource(): View = rootLayout

    private val pathToSave = Environment.getExternalStorageDirectory().path + "/htdocs/signage"
    private val pathFolder1 = Environment.getExternalStorageDirectory().path + "/htdocs"
    private val pathFolder2 = Environment.getExternalStorageDirectory().path + "/htdocs/signage"

    private val database = FirebaseDatabase.getInstance()
    private lateinit var myRef: DatabaseReference
    private val listContents: MutableList<String> = mutableListOf()
    private val hashMapUpdate = hashMapOf<String, Int>()
    private var isForceDownload = false
    private lateinit var timerTimeline: CountDownTimer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        checkPermission({ initInstance() }, 1122)
    }

    override fun onStart() {
        super.onStart()
        rootLayout.requestFocus()
        layoutCountDown.isGone = isForceDownload
        when {
            autoDownload.isChecked -> {
                timerTimeline = object : CountDownTimer(4000L, 1000) {
                    override fun onTick(millisUntilFinished: Long) {
                        println("Count Down auto player video duration = ${millisUntilFinished / 1000}")
                        tvCountDown.text =
                                getString(R.string.text_count_down, millisUntilFinished.toInt() / 1000)
                    }

                    override fun onFinish() {
                        logHelper("onFinish Count Down")
                        tvCountDown.text = "Video Starting"
                        if (isNetworkConnectedOrConnecting()) {
                            checkAndDownloadContent()
                        } else {
                            toast("internet not available")
                            if (SharePrefManager.getIntFromPref(PLAY_MODE) == 1 && btnSDVideoPlayer.isEnabled)
                                delayFunction(500) { openSDVideoPlayer() }
                            else
                                delayFunction(500) { openVideoPlayer() }
                        }
                    }
                }
                timerTimeline.start()
            }
            else -> {
                layoutCountDown.isGone = true
                tvCountDown.isGone = true
            }
        }
        if (isNetworkConnectedOrConnecting()) {
            database.getReference(BuildConfig.PREFIX_TYPE)
                    .child(branchName.toLowerCase())
                    .child(directoryName.toLowerCase())
                    .child("is_playing").setValue(0)
        }
    }

    private fun showErrorConnection(msg: String = "FTP Error") {
        layoutProgress?.isInvisible = true
        tvLoading?.text = msg
    }

    private fun initInstance() {
        isForceDownload = intent.getBooleanExtra("force_download", false)
        tvVersion?.text = "V. " + BuildConfig.VERSION_NAME
        tvMall?.text = "$branchName - $directoryName"

//        logHelper(isExternalStorageReadOnly.toString() + isExternalStorageAvailable.toString())
        btnSDVideoPlayer.isEnabled = !isExternalStorageReadOnly && isExternalStorageAvailable &&
                (UtilsInJava.getExternalStoragePath(this, true) != null)
        btnSDVideoPlayer.setOnClickListener { openSDVideoPlayer() }

        btnLoadContent.setOnClickListener { loadListContentAsset { updateFileAssetAndContent { setValueAfterDownloadCompleted() } } }
        btnLoadAll.setOnClickListener { loadListContentAsset { downloadFileFromFTP() } }
        btnNewSetting.setOnClickListener {
            SharePrefManager.saveBooleanToPref("auto_download", false)
            SharePrefManager.saveBooleanToPref(IS_SETUP, false)
            startActivity(Intent(this, SetupActivity::class.java))
            finish()
        }

        btnVideoPlayer.setOnClickListener { openVideoPlayer() }
        layoutCountDown.setOnClickListener {
            if (::timerTimeline.isInitialized) timerTimeline.cancel()
            layoutCountDown.isGone = true
        }

        autoDownload.isChecked = SharePrefManager.getBooleanFromPref("auto_download", false)
        autoDownload.setOnCheckedChangeListener { _, isChecked ->
            SharePrefManager.saveBooleanToPref("auto_download", isChecked)
        }
        autoStart.isChecked = SharePrefManager.getBooleanFromPref("autoStart", false)
        autoStart.setOnCheckedChangeListener { _, isChecked ->
            setAlwaysStartApp(isChecked)
        }
        setAlwaysStartApp(autoStart.isChecked)

        if (!File(pathFolder1).exists()) File(pathFolder1).mkdir()
        if (!File(pathFolder2).exists()) File(pathFolder2).mkdir()
        if (!File("$pathFolder2/$ASSET_DIR_NAME").exists()) File("$pathFolder2/$ASSET_DIR_NAME").mkdir()

        if (isNetworkConnectedOrConnecting())
            startObserveData(branchName, directoryName)
        else
            toast("internet not available")
    }

    private fun setAlwaysStartApp(isCheck: Boolean) {
        SharePrefManager.saveBooleanToPref("autoStart", isCheck)
        val serviceIntent = Intent(this, AwesomeBackgroundService::class.java)
        if (isCheck) {
            (application as MainApplication).startIntervalAlarm()
            serviceIntent.putExtra(AwesomeBackgroundService.APPLICATION_STATUS, MainApplication.APP_START)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                ContextCompat.startForegroundService(this, serviceIntent)
            } else {
                startService(serviceIntent)
            }
        } else {
            (application as MainApplication).stopIntervalAlarm()
            stopService(serviceIntent)
        }
    }

    private fun loadListContentAsset(done: () -> Unit) {
        layoutProgress?.isVisible = true
        tvLoading?.isVisible = true
        tvLoading?.text = "Loading..."

        val service = ServiceManager.service
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val response = withContext(Dispatchers.IO) {
                    service.getListContent(branchName, directoryName)
                }
                if (response.isSuccessful) {
                    //Do something with response e.g show to the UI.
                    val content = response.body() ?: listOf()
                    logHelper(GsonHelper.getGson().toJson(content))

                    if (content.isEmpty()) {
                        layoutProgress?.isVisible = false
                        tvLoading?.text = "No Content"
                        return@launch
                    }

                    listContents.clear()
                    listContents.addAll(content)
                    done.invoke()
                } else {
                    toast("Error: ${response.code()}")
                }
            } catch (e: HttpException) {
                toast("Exception ${e.message}")
                showErrorConnection("HttpException")
            } catch (e: Throwable) {
                toast("Ooops: Something else went wrong")
                showErrorConnection(e.localizedMessage)
            }
        }
    }

    private fun downloadFileFromFTP() {
        val ftp = connectingWithFTP(name, user, pass)
        if (ftp == null) {
            showErrorConnection()
            return
        }
        delayIOFunction(2000) {
            val connectionContent = downloadMultipleFiles(
                    ftp,
                    "$pathToSave/$ASSET_DIR_NAME",
                    CONTENT_DIR_NAME,
                    CONTENT_DIR_NAME,
                    listContents
            )
            val connectionMall = downloadMultipleFiles(
                    ftp,
                    pathToSave,
                    "/${branchName.toLowerCase()}/${directoryName.toLowerCase()}",
                    "/${branchName.toLowerCase()}/${directoryName.toLowerCase()}"
            )
            val connectionPlayer =
                    downloadMultipleFiles(ftp, pathToSave, PLAYER_DIR_NAME, PLAYER_DIR_NAME)
            closeFTP(ftp)

            delayFunction(500) {
//                val log = "Download Status : asset - $connectionContent / directory - $connectionMall / player - $connectionPlayer"
                layoutProgress?.isInvisible = true
                tvLoading?.text = "Load Complete"

                /** set firebase when download complete **/
                setValueAfterDownloadCompleted()
            }
        }
    }

    private fun compareAndDeleteUnUsedFile(done: () -> Unit) {
        delayFunction(500) {
            layoutProgress?.isVisible = true
            tvLoading?.text = "Comparing File"
        }
        val ftp = connectingWithFTP(name, user, pass)
        if (ftp == null) {
            showErrorConnection()
            return
        }
        delayIOFunction(1500) {
            val connectionContent = checkMultipleFiles(
                    ftp,
                    pathToSave,
                    CONTENT_DIR_NAME,
                    CONTENT_DIR_NAME,
                    "/$ASSET_DIR_NAME",
                    listContents
            )
            val connectionMall = checkMultipleFiles(
                    ftp,
                    pathToSave,
                    "/${branchName.toLowerCase()}/${directoryName.toLowerCase()}",
                    "/${branchName.toLowerCase()}/${directoryName.toLowerCase()}"
            )
            val connectionPlayer =
                    checkMultipleFiles(ftp, pathToSave, PLAYER_DIR_NAME, PLAYER_DIR_NAME)
            closeFTP(ftp)

            delayFunction(500) {
//                val log = "Checked Status : asset - $connectionContent / directory - $connectionMall / player - $connectionPlayer"
                layoutProgress?.isInvisible = true
                tvLoading?.text = "Deleted Unused"

                compareDifFile(pathToSave)
                done.invoke()
            }
        }
    }

    private fun updateFilePlayer(done: () -> Unit) {
        layoutProgress?.isVisible = true
        tvLoading?.isVisible = true
        tvLoading?.text = "Loading Player..."
        val ftp = connectingWithFTP(name, user, pass)
        if (ftp == null) {
            showErrorConnection()
            return
        }
        delayIOFunction(2000) {

            val connectionPlayer =
                    downloadMultipleFiles(ftp, pathToSave, PLAYER_DIR_NAME, PLAYER_DIR_NAME)
            closeFTP(ftp)

            delayFunction(500) {
//                val log = "Download Status : asset - false / directory - false / player - $connectionPlayer"
                layoutProgress?.isInvisible = true
                tvLoading?.text = "Load Player Complete"
                done.invoke()
            }
        }
    }

    private fun updateFileAssetAndContent(done: () -> Unit) {
        layoutProgress?.isVisible = true
        tvLoading?.isVisible = true
        tvLoading?.text = "Loading..."
        val ftp = connectingWithFTP(name, user, pass)
        if (ftp == null) {
            layoutProgress?.isInvisible = true
            tvLoading?.text = "FTP Error"
            return
        }
        delayIOFunction(2000) {

            val connectionContent = downloadMultipleFiles(
                    ftp,
                    "$pathToSave/$ASSET_DIR_NAME",
                    CONTENT_DIR_NAME,
                    CONTENT_DIR_NAME,
                    listContents
            )
            val connectionMall = downloadMultipleFiles(
                    ftp,
                    pathToSave,
                    "/${branchName.toLowerCase()}/${directoryName.toLowerCase()}",
                    "/${branchName.toLowerCase()}/${directoryName.toLowerCase()}"
            )
            closeFTP(ftp)

            delayFunction(500) {
//                val log = "Download Status : asset - $connectionContent / directory - $connectionMall / player -false"
                layoutProgress?.isInvisible = true
                tvLoading?.text = "Loading Complete"
                done.invoke()
            }
        }
    }

    private fun updateFileContent(done: () -> Unit) {
        layoutProgress?.isVisible = true
        tvLoading?.isVisible = true
        tvLoading?.text = "Content Loading..."
        val ftp = connectingWithFTP(name, user, pass)
        if (ftp == null) {
            showErrorConnection()
            return
        }
        delayIOFunction(2000) {

            val connectionMall = downloadMultipleFiles(
                    ftp,
                    pathToSave,
                    "/${branchName.toLowerCase()}/${directoryName.toLowerCase()}",
                    "/${branchName.toLowerCase()}/${directoryName.toLowerCase()}"
            )
            closeFTP(ftp)

            delayFunction(500) {
//                val log = "Download Status : directory - $connectionMall / player -false"
                layoutProgress?.isInvisible = true
                tvLoading?.text = "Load Content Complete"
                done.invoke()
            }
        }
    }

    private fun startObserveData(branchName: String, directoryName: String) {
        myRef = database.getReference(BuildConfig.PREFIX_TYPE).child(branchName.toLowerCase())
                .child(directoryName.toLowerCase())

        myRef.addChildEventListener(childEventListener)
    }

    private val childEventListener = object : ChildEventListener {
        override fun onCancelled(datebaseError: DatabaseError) {
            logHelper(datebaseError.message)
        }

        override fun onChildMoved(dataSnapshot: DataSnapshot, previousChildName: String?) {
        }

        override fun onChildChanged(dataSnapshot: DataSnapshot, previousChildName: String?) {
            if (dataSnapshot.key in LIST_KEY) {
                val value = dataSnapshot.getValue(Int::class.java)
                logHelper("onChildChanged - " + dataSnapshot.key + " : " + value.toString())
                when (dataSnapshot.key) {
                    NEED_RELOAD_PLAYER -> { // set value to tell play for restart load content.
                        hashMapUpdate[NEED_RELOAD_PLAYER] = value!!
                        if (value == 1) {
                            updateFileContent {
                                if (SharePrefManager.getIntFromPref(PLAY_MODE) == 1 && btnSDVideoPlayer.isEnabled)
                                    openSDVideoPlayer()
                                else
                                    openVideoPlayer()
                            }
                        }
                    }
                    NEED_UPDATE_PLAYER -> { //  download or update modified file from ftp folder player.
                        if (value == 1) {
                            updateFilePlayer { setValueAfterDownloadCompleted() }
                        }
                    }
                    NEED_DOWNLOAD_CONTENT -> { //   download or update modified file from ftp folder branch and folder directory.
                        if (value == 1) {
                            loadListContentAsset { updateFileAssetAndContent { setValueAfterDownloadCompleted() } }
                        }
                    }
                    NEED_CLEAN_TRASH -> { //    delete file that wasn't in ftp from local storage.
                        if (value == 1) {
                            compareAndDeleteUnUsedFile { delayFunction(2000) { disableDeleteContent() } }
                        }
                    }
                }
            }
        }

        override fun onChildAdded(dataSnapshot: DataSnapshot, previousChildName: String?) {
            if (dataSnapshot.key in LIST_KEY) {
                val value = dataSnapshot.value.toString().toIntOrNull()
                logHelper("onChildAdded - " + dataSnapshot.key + " : " + (dataSnapshot.value.toString()
                        .toIntOrNull() ?: dataSnapshot.value.toString())
                )
                value?.let {
                    when (dataSnapshot.key) {
                        NEED_RELOAD_PLAYER -> { // set value to tell play for restart load content.
                            hashMapUpdate[NEED_RELOAD_PLAYER] = it
                        }
                        PLAY_MODE -> {
                            SharePrefManager.saveIntToPref(PLAY_MODE, it)
                        }
                    }
                }
            }
        }

        override fun onChildRemoved(dataSnapshot: DataSnapshot) {
        }
    }

    private fun checkAndDownloadContent() {
        myRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {

            }

            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.hasChildren()) {
                    hashMapUpdate.clear()
                    snapshot.children.forEach {
                        if (it.key!! in LIST_KEY) {
                            val value = it.value.toString().toIntOrNull()
                            value?.let { data ->
                                hashMapUpdate[it.key!!] = data
                            }
                        }
                    }

                    if (hashMapUpdate.isNotEmpty()) {
                        hashMapUpdate.remove(NEED_CLEAN_TRASH)
                        if (hashMapUpdate[NEED_DOWNLOAD_CONTENT] == 0 && hashMapUpdate[NEED_UPDATE_PLAYER] == 0 && hashMapUpdate[NEED_RELOAD_PLAYER] == 0) {
                            if (isForceDownload) {
                                isForceDownload = false
                                delayFunction(500) { loadListContentAsset { downloadFileFromFTP() } }
                            } else {
                                if (SharePrefManager.getIntFromPref(PLAY_MODE) == 1 && btnSDVideoPlayer.isEnabled)
                                    delayFunction(500) { openSDVideoPlayer() }
                                else
                                    delayFunction(500) { openVideoPlayer() }
                            }
                        } else if (hashMapUpdate[NEED_DOWNLOAD_CONTENT] == 1 && hashMapUpdate[NEED_UPDATE_PLAYER] == 1) {
                            loadListContentAsset { downloadFileFromFTP() }
                        } else if (hashMapUpdate[NEED_DOWNLOAD_CONTENT] == 1 && hashMapUpdate[NEED_UPDATE_PLAYER] == 0) {
                            loadListContentAsset { updateFileAssetAndContent { setValueAfterDownloadCompleted() } }
                        } else if (hashMapUpdate[NEED_DOWNLOAD_CONTENT] == 0 && hashMapUpdate[NEED_UPDATE_PLAYER] == 1) {
                            updateFilePlayer { setValueAfterDownloadCompleted() }
                        } else if (hashMapUpdate[NEED_RELOAD_PLAYER] == 1) {
                            updateFileContent {
                                if (SharePrefManager.getIntFromPref(PLAY_MODE) == 1 && btnSDVideoPlayer.isEnabled)
                                    openSDVideoPlayer()
                                else
                                    openVideoPlayer()
                            }
                        }
                    }
                }
            }
        })
    }

    private fun resetAllFirebaseValue() {
        myRef.child(NEED_RELOAD_PLAYER).setValue(0)
        myRef.child(NEED_UPDATE_PLAYER).setValue(0)
        myRef.child(NEED_DOWNLOAD_CONTENT).setValue(0)
        myRef.child(NEED_CLEAN_TRASH).setValue(0)
    }

    private fun setValueAfterDownloadCompleted() {
        compareAndDeleteUnUsedFile {
            delayFunction(1000) {
                disableDeleteContent()
                disableDownloadContent()
                disableUpdatePlayer()
                if (hashMapUpdate[NEED_RELOAD_PLAYER] != null && hashMapUpdate[NEED_RELOAD_PLAYER] == 1) {
                    if (SharePrefManager.getIntFromPref(PLAY_MODE) == 1 && btnSDVideoPlayer.isEnabled)
                        openSDVideoPlayer()
                    else
                        openVideoPlayer()
                    return@delayFunction
                }
                enableReloadPlayer()
            }
        }
    }

    private fun disableUpdatePlayer() {
        myRef.child(NEED_UPDATE_PLAYER).setValue(0)
    }

    private fun disableDownloadContent() {
        myRef.child(NEED_DOWNLOAD_CONTENT).setValue(0)
    }

    private fun disableDeleteContent() {
        myRef.child(NEED_CLEAN_TRASH).setValue(0)
    }

    private fun enableReloadPlayer() {
        myRef.child(NEED_RELOAD_PLAYER).setValue(1)
    }

    private fun openVideoPlayer() {
        tvLoading?.isInvisible = true
        val i = Intent(this, VideoPlayerActivity::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(i)
    }

    private fun openSDVideoPlayer() {
        tvLoading?.isInvisible = true
        val i = Intent(this, SDVideoPlayerActivity::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(i)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::timerTimeline.isInitialized) timerTimeline.cancel()
        if (::myRef.isInitialized) myRef.removeEventListener(childEventListener)
    }

    private fun checkPermission(unit: () -> Unit, code: Int) {
        val permissionCheck =
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            unit()
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), code)
            }
        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1122 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    initInstance()
                else {
                    Toast.makeText(this, "Please grant storage permission", Toast.LENGTH_SHORT)
                            .show()
                    layoutProgress?.isInvisible = true
                    tvLoading?.text = "Loading Error"
                }
            }
            else -> return
        }
    }

    override fun onBackPressed() {
        if (layoutProgress.isVisible)
            return
        super.onBackPressed()
    }
}
