package com.example.pushsignage.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.example.pushsignage.MainApplication
import com.example.pushsignage.R
import com.example.pushsignage.util.logHelper
import com.example.pushsignage.util.restartApp

class AwesomeBackgroundService : Service() {

    companion object {
        const val NOTIFICATION_ID = 123
        const val NOTIFICATION_CHANNEL = "app_status"
        const val NOTIFICATION_NAME = "Status"
        const val APPLICATION_STATUS = "app_status"
    }

    private var lastState = -1

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        logHelper("create service")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notification = createProgressNotification("Setup")
            createNotificationChannel()
            startForeground(NOTIFICATION_ID, notification)
        } else {
            val notification = createProgressNotification("Setup")
            getNotificationManager().notify(NOTIFICATION_ID, notification)
            startForeground(NOTIFICATION_ID, notification)
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        logHelper("start service lastState = $lastState")
        if (intent == null) {
            checkStateAndRestartApp(true)
            return START_NOT_STICKY
        }
        if (!intent.hasExtra(APPLICATION_STATUS)) return START_NOT_STICKY
        val state = intent.getIntExtra(APPLICATION_STATUS, MainApplication.APP_DESTROY)
        // check state to restart only stop not destroy
        if (lastState == state || lastState == MainApplication.APP_STOP && state == MainApplication.APP_DESTROY) return START_STICKY
        logHelper("AwesomeBackgroundService lastState = $lastState ,state = $state")
        lastState = state
        val msg = when (state) {
            MainApplication.APP_CREATE, MainApplication.APP_START -> "Online"
            else -> "Offline"
        }

        val notification = createProgressNotification(msg)
        startForeground(NOTIFICATION_ID, notification)

        checkStateAndRestartApp(state == MainApplication.APP_STOP || state == MainApplication.APP_DESTROY)

        return START_STICKY
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel() {
        val channel = NotificationChannel(
            NOTIFICATION_CHANNEL,
            NOTIFICATION_NAME,
            NotificationManager.IMPORTANCE_HIGH
        ).apply {
            enableVibration(true)
            enableLights(true)
        }
        getNotificationManager().createNotificationChannel(channel)
    }


    private fun createProgressNotification(status: String): Notification {
        return NotificationCompat.Builder(this, NOTIFICATION_CHANNEL).apply {
            setContentTitle("Application Status")
            setContentText(status)
            setSmallIcon(R.mipmap.ic_launcher)
            setOnlyAlertOnce(true)
            setDefaults(NotificationCompat.DEFAULT_ALL)
            priority = NotificationCompat.PRIORITY_HIGH
        }.build()
    }

    private fun checkStateAndRestartApp(state: Boolean) {
        if (state) restartApp()
    }

    private fun getNotificationManager(): NotificationManager {
        return getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        super.onTaskRemoved(rootIntent)
        lastState = -1
        logHelper("onTaskRemoved")
    }
}