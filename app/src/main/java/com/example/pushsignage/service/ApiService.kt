package com.example.pushsignage.service

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("getgssetslist.php")
    suspend fun getListContent(@Query("mall") mall: String, @Query("unit") unit: String): Response<List<String>>
}