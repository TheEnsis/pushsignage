package com.example.pushsignage.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.content.ContextCompat
import com.example.pushsignage.MainApplication
import com.example.pushsignage.util.toast

class BackgroundAlarmReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
//        context?.toast("BackgroundAlarmReceiver")
        val serviceIntent = Intent(context, AwesomeBackgroundService::class.java)
        intent?.putExtra(AwesomeBackgroundService.APPLICATION_STATUS, MainApplication.APP_STOP)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context?.let { ContextCompat.startForegroundService(it, serviceIntent) }
        } else {
            context?.startService(serviceIntent)
        }
    }
}