package com.example.pushsignage.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.pushsignage.SetupActivity
import com.example.pushsignage.util.SharePrefManager
import com.example.pushsignage.util.delayFunction
import com.example.pushsignage.util.logHelper
import com.example.pushsignage.util.toast

class BackgroundBootReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        context?.toast("ACTION_BOOT_COMPLETED")
        logHelper("ACTION_BOOT_COMPLETED")
        if (SharePrefManager.getBooleanFromPref("autoStart", false)) {
            if (Intent.ACTION_BOOT_COMPLETED == intent?.action) {
                context?.let { c ->
                    Intent(c, SetupActivity::class.java).apply {
                        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED)
                    }.also {
                        delayFunction(3000) { c.startActivity(it) }
                    }
                }
//                val serviceIntent = Intent(context, AwesomeBackgroundService::class.java)
//                serviceIntent.putExtra(AwesomeBackgroundService.APPLICATION_STATUS, MainApplication.APP_STOP)
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                    context?.let { ContextCompat.startForegroundService(it, serviceIntent) }
//                } else {
//                    context?.startService(serviceIntent)
//                }
            }
        } else {
            context?.toast("Not restart")
        }
    }
}