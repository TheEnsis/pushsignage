package com.example.pushsignage

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.provider.Settings
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.pushsignage.util.SharePrefManager
import com.example.pushsignage.util.checkPermission
import com.example.pushsignage.util.toast
import com.google.android.gms.security.ProviderInstaller
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_setup.*
import java.io.File


class SetupActivity : BaseActivity() {

    private val pathFolderMain = Environment.getExternalStorageDirectory().path + "/htdocs"

    override fun viewLayoutResource(): View = rootLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (SharePrefManager.getBooleanFromPref(IS_SETUP)) {
            startMainPage(force = false)
        }

        setContentView(R.layout.activity_setup)

        checkSystemDate()
        initInstance()

        try {
            ProviderInstaller.installIfNeeded(applicationContext)
        } catch (e: Exception) {
            toast(e.toString())
        }
    }

    private fun checkSystemDate() {
        val timeSettings = Settings.Global.getString(
                this.contentResolver,
                Settings.Global.AUTO_TIME
        )
        if (timeSettings!!.contentEquals("0")) {
            val alertDialog: AlertDialog = AlertDialog.Builder(this).create()
            alertDialog.setTitle("Alert")
            alertDialog.setMessage("กรุณาปรับวันเดือนปีของเครื่องให้เป็นปัจจุบัน")
            alertDialog.setButton(
                    AlertDialog.BUTTON_POSITIVE, "OK"
            ) { dialog, which -> dialog.dismiss() }
            alertDialog.show()
        }
    }

    private fun initInstance() {
        rootLayout.requestFocus()
        tvVersion.text = "V. " + BuildConfig.VERSION_NAME

        val branchName = SharePrefManager.getStringFromPref(FTP_BRANCH)
        val directoryName = SharePrefManager.getStringFromPref(FTP_DIRECTORY)

        val hostName = SharePrefManager.getStringFromPref(FTP_HOST)
        val userName = SharePrefManager.getStringFromPref(FTP_USER)
        val userPass = SharePrefManager.getStringFromPref(FTP_PASS)

        if (hostName.isNotEmpty()) etHost?.setText(hostName)
        if (userName.isNotEmpty()) etUser?.setText(userName)
        if (userPass.isNotEmpty()) etPass?.setText(userPass)

        etBranch.setText(branchName)
        etDirectory.setText(directoryName)

        btnOk.setOnClickListener {
//                                    throw RuntimeException("Test Crash")
            if (etHost.text.toString().isEmpty() || etUser.text.toString().isEmpty() ||
                    etPass.text.toString().isEmpty() || etBranch.text.toString().isEmpty() ||
                    etDirectory.text.toString().isEmpty()
            ) {
                toast("data invalid")
                return@setOnClickListener
            }
            saveDataToLocal()
            checkPermission({
                if (!File(pathFolderMain).exists()) File(pathFolderMain).mkdir()
                saveDataToStorage()
            }, 1122)
        }
    }

    private fun saveDataToLocal() {
        SharePrefManager.saveStringToPref(FTP_HOST, etHost.text.toString())
        SharePrefManager.saveStringToPref(FTP_USER, etUser.text.toString())
        SharePrefManager.saveStringToPref(FTP_PASS, etPass.text.toString())
        SharePrefManager.saveStringToPref(FTP_BRANCH, etBranch.text.toString().toLowerCase())
        SharePrefManager.saveStringToPref(FTP_DIRECTORY, etDirectory.text.toString().toLowerCase())
    }

    private fun saveDataToStorage() {
        val mall = etBranch.text.toString().toLowerCase()
        val unit = etDirectory.text.toString().toLowerCase()
        val savePath = "$pathFolderMain/config.xml"
        val saveFile = File(savePath)
        if (!saveFile.exists()) saveFile.createNewFile()

        val content = "<root>\n" +
                "    <mall>$mall</mall>\n" +
                "    <unit>$unit</unit>\n" +
                "    <targetfolder>C:\\xampp\\htdocs\\signage\\</targetfolder>\n" +
                "</root>"

        saveFile.writeText(content)

        SharePrefManager.saveBooleanToPref(IS_SETUP, true)

        initializeFirebaseDatabase(mall, unit)

        startMainPage(force = true)

    }

    private fun initializeFirebaseDatabase(branchName: String, directoryName: String) {
        val prefixRef = BuildConfig.PREFIX_TYPE
        val myRef = FirebaseDatabase.getInstance().getReference(prefixRef)
                .child(branchName.toLowerCase()).child(directoryName.toLowerCase())
        myRef.child(NEED_CAPTURE_SCREEN).setValue(0)
        myRef.child(NEED_RELOAD_PLAYER).setValue(0)
        myRef.child(NEED_UPDATE_PLAYER).setValue(0)
        myRef.child(NEED_DOWNLOAD_CONTENT).setValue(0)
        myRef.child(NEED_CLEAN_TRASH).setValue(0)
    }

    private fun startMainPage(force: Boolean) {
        val mainIntent = Intent(this, MainActivity::class.java)
        mainIntent.putExtra("force_download", force)
        startActivity(mainIntent)
        finish()
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1122 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (!File(pathFolderMain).exists()) File(pathFolderMain).mkdir()
                    saveDataToStorage()
                } else
                    Toast.makeText(
                            this,
                            "Please grant storage permission",
                            Toast.LENGTH_SHORT
                    ).show()
            }
            else -> return
        }
    }
}
