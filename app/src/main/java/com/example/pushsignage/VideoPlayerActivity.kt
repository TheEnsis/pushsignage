package com.example.pushsignage

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.core.net.toUri
import androidx.core.view.isNotEmpty
import androidx.core.view.updateMargins
import com.bumptech.glide.Glide
import com.example.pushsignage.data.*
import com.example.pushsignage.util.*
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.google.firebase.database.FirebaseDatabase
import com.google.gson.JsonSyntaxException
import kotlinx.android.synthetic.main.activity_video_player.*
import org.joda.time.Interval
import java.io.File
import kotlin.collections.set

class VideoPlayerActivity : BaseActivity() {

    override fun viewLayoutResource(): View = rootLayout

    private val mediaDataSourceFactory: DataSource.Factory by lazy { DefaultDataSourceFactory(this, Util.getUserAgent(this, BuildConfig.APPLICATION_ID)) }

    private var allContent = AllContentModel()
    private var allTimelineElement = mutableListOf<TimelineElement>()
    private var allPlaylist = mutableListOf<Playlist>()

    private var selectedCampaign = Campaign()
    private var selectedElement = TimelineElement()

    private val basePath = Environment.getExternalStorageDirectory().path + "/htdocs/signage"
    private val contentPath = "/content.json"
    private var width = 0
    private var height = 0

    private var currentElementIndex = 0
    private lateinit var timerTimeline: CountDownTimer
    private val listTimerContent = mutableListOf<CountDownTimer>()
    private val hashMapVideoPlayer = hashMapOf<Int, SimpleExoPlayer>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_player)
        initSystemUI()
        initInstance()
    }

    private fun initSystemUI() {
        hideSystemUI()

        window.decorView.setOnSystemUiVisibilityChangeListener { visibility ->
            // Note that system bars will only be "visible" if none of the
            // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
            if (visibility and View.SYSTEM_UI_FLAG_FULLSCREEN == 0) {
                // The system bars are visible. Make any desired
                // adjustments to your UI, such as showing the action bar or
                // other navigational controls.
                delayFunction(1500) { hideSystemUI() }
                Log.e("decorView", "The system bars are visible")
            } else {
                // The system bars are NOT visible. Make any desired
                // adjustments to your UI, such as hiding the action bar or
                // other navigational controls.
                Log.e("decorView", "The system bars are NOT visible")
            }
        }
    }

    private fun initInstance() {
        readContentFromLocalStorage()

        if (allContent.campaign.isNotEmpty()) {
            val size = getDisplaySize()
            width = size.x
            height = size.y
            initCampaign()
        }

        FirebaseDatabase.getInstance().getReference(BuildConfig.PREFIX_TYPE)
                .child(branchName.toLowerCase())
                .child(directoryName.toLowerCase())
                .child(NEED_RELOAD_PLAYER).setValue(0)

    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        logHelper("onNewIntent")
        if (::timerTimeline.isInitialized) timerTimeline.cancel()
        hashMapVideoPlayer.values.forEach { it.release() }
        listTimerContent.forEach { it.cancel() }
        delayFunction(2000) { initInstance() }
    }

    private fun readContentFromLocalStorage() {
        if (!File(basePath + contentPath).exists()) return
        val inputString: String = File(basePath + contentPath).bufferedReader().use { it.readText() }
        logHelper(inputString)
        try {
            allContent = GsonHelper.getGson().fromJson(inputString, AllContentModel::class.java)
        } catch (e: JsonSyntaxException) {
            toast("json invalid")
        }
    }

    private fun initCampaign() {
        if (!isFinishing && !isDestroyed) {
            val campaign = checkDateTimeCampaign()
            val havePlayer = checkDateTimePlayList()
            if (campaign != null) {
                if (havePlayer) {
                    if (::timerTimeline.isInitialized) timerTimeline.cancel()
                    selectedCampaign = campaign
                    initTimeLine(campaign.timelineElement)
                } else {
                    toast("no player available in start - end datetime")
                    delayFunction(60000) { initCampaign() }
                }
            } else {
                toast("no content available in start - end datetime")
                delayFunction(60000) { initCampaign() }
            }
        } else {
//            toast("restart")
            logHelper("restart")
            finish()
        }
    }

    private fun checkDateTimeCampaign(): Campaign? {
        return allContent.campaign
                .find { getDateTimeInstantFormat(it.startDate, it.startTime).isBeforeNow && getDateTimeInstantFormat(it.endDate, it.endTime).isAfterNow }
                ?.takeIf { getTimeInstantFormat(it.startTime).isBeforeNow && getTimeInstantFormat(it.endTime).isAfterNow }
    }

    private fun checkDateTimePlayList(): Boolean {
        return allContent.playList.isNotEmpty() && allContent.playList.flatMap { it.contents }.any { checkDateTimePlayerContent(it) }
    }

    private fun initTimeLine(timeliness: List<TimelineElement>) {
        allTimelineElement.clear()
        allTimelineElement.addAll(timeliness)
        allPlaylist.clear()
        allPlaylist.addAll(allContent.playList)

        selectedElement = allTimelineElement[currentElementIndex]

        initLayoutElement()

        startCountDownTimelineDuration()
    }

    private fun initLayoutElement() {
        val currentLayout = allContent.layouts.find { it.layoutId == selectedElement.layoutId }

        currentLayout?.let { layouts ->
            logHelper(layouts.toString())

            /** set list slot from layout **/
            val allSlot = mutableListOf<Slot?>()
            selectedElement.slots.map { es -> currentLayout.slots.find { it.slotId == es.slotId } }.toCollection(allSlot)

            /** start check layout resolution **/
            val widthFactor: Double
            val heightFactor: Double

            if (currentLayout.width.isNullOrEmpty() or currentLayout.height.isNullOrEmpty()) {
                widthFactor = 1.0
                heightFactor = 1.0
            } else {
                widthFactor = width / currentLayout.width!!.replace("px", "").toDouble()
                heightFactor = height / currentLayout.height!!.replace("px", "").toDouble()
                logHelper("width = $width - height = $height / widthFactor = $widthFactor - heightFactor = $heightFactor")
            }
            /** end check layout resolution **/

            /** set color for root layout**/
            if (currentLayout.backgroundColor.isNotEmpty()) rootLayout.setBackgroundColor(Color.parseColor(currentLayout.backgroundColor))


            /** set list playlist **/
            val myPlaylist = mutableListOf<Playlist>()
            selectedElement.slots.map { es -> allPlaylist.find { it.playlistId == es.playlistId }!! }.toCollection(myPlaylist)

            rootLayout.removeAllViews()
            listTimerContent.forEach { it.cancel() }
            hashMapVideoPlayer.values.forEach { it.release() }

            allSlot.sortedBy { it?.zIndex ?: 0 }.forEachIndexed { _, slot ->
                slot?.let { ls ->
                    val layoutPlayer = FrameLayout(this)
                    val layoutParams = RelativeLayout.LayoutParams(
                            (ls.width.fromPxToDouble() * widthFactor).toInt(),
                            (ls.height.fromPxToDouble() * heightFactor).toInt()
                    )
                    layoutParams.updateMargins(
                            left = (ls.x.fromPxToInt() * widthFactor).toInt(),
                            top = (ls.y.fromPxToInt() * heightFactor).toInt()
                    )
                    layoutPlayer.layoutParams = layoutParams

                    /** find current player and check type **/
                    val selectedPlayer = myPlaylist.find { playlist -> playlist.playlistId == (selectedElement.slots.find { it.slotId == ls.slotId })!!.playlistId }!!

                    initPlayer(layoutPlayer, layoutParams, selectedPlayer)
                    rootLayout.addView(layoutPlayer)
                }
            }
        }
    }

    private fun initPlayer(layoutPlayer: FrameLayout, layoutParams: RelativeLayout.LayoutParams, selectedPlayer: Playlist, index: Int = 0) {
        val currentContent = selectedPlayer.contents[index]
        if (checkDateTimePlayerContent(currentContent)) {
            println("checkDateTimePlayerContent pass")
            val view = initPlayerContent(currentContent)
            if (view != null) {
                println("add view pass")
                if (layoutPlayer.isNotEmpty()) layoutPlayer.removeAllViews()
                layoutPlayer.addView(view)
                startCountDownContentDuration(layoutPlayer, layoutParams, selectedPlayer, index)
            } else {
                toast("no file available in storage")
                val newIndex = if (index + 1 < selectedPlayer.contents.size) index + 1 else 0
                initPlayer(layoutPlayer, layoutParams, selectedPlayer, newIndex)
            }
        } else {
            val newIndex = if (index + 1 < selectedPlayer.contents.size) index + 1 else 0
            initPlayer(layoutPlayer, layoutParams, selectedPlayer, newIndex)
        }
    }

    private fun checkDateTimePlayerContent(currentContent: Content): Boolean {
        currentContent.also { content ->
            val fsDate = content.fileStartDate ?: ""
            val esDate = content.fileEndDate ?: ""

            val fDate = content.startDate ?: ""
            val eDate = content.endDate ?: ""
            logHelper("currentContent = ${currentContent.mediaId} fsDate = $fsDate esDate = $esDate fDate = $fDate eDate = $eDate")
            return when (fsDate.isNotEmpty() && esDate.isNotEmpty() && fDate.isNotEmpty() && eDate.isNotEmpty()) {
                true -> {
                    val range1 = Interval(getDateInstantFormat(fsDate), getDateInstantFormat(esDate))
                    val range2 = Interval(getDateInstantFormat(fDate), getDateInstantFormat(eDate))
                    val overlapRange = range1.overlap(range2)
                    logHelper(overlapRange.start.plusDays(1).toString() + " " + overlapRange.end.plusDays(1).plusHours(23).plusMinutes(59).plusSeconds(59).toString())
                    return (overlapRange.start.plusDays(1).isBeforeNow && overlapRange.end.plusDays(1).plusHours(23).plusMinutes(59).plusSeconds(59).isAfterNow)
                }
                false -> {
                    if (fsDate.isNotEmpty() && esDate.isNotEmpty() && fDate.isEmpty() && eDate.isEmpty()) {
//                        logHelper("fsDate.isNotEmpty $fsDate && esDate.isNotEmpty $esDate " + (getDateInstantFormat(fsDate).isBeforeNow && getDateTimeInstantFormat(esDate, "23:59:59").isAfterNow).toString())
                        getDateInstantFormat(fsDate).isBeforeNow && getDateTimeInstantFormat(esDate, "23:59:59").isAfterNow

                    } else if (fsDate.isEmpty() && esDate.isEmpty() && fDate.isNotEmpty() && eDate.isNotEmpty()) {
//                        logHelper("fDate.isNotEmpty $fDate && eDate.isNotEmpty $eDate " + (getDateInstantFormat(fDate).isBeforeNow && getDateTimeInstantFormat(eDate, "23:59:59").isAfterNow).toString())
                        getDateInstantFormat(fDate).isBeforeNow && getDateTimeInstantFormat(eDate, "23:59:59").isAfterNow

                    } else {
                        true
                    }
                }
            }
        }
    }

    private fun initPlayerContent(currentContent: Content): View? {
        currentContent.also { content ->
            if (content.type == "image") {
                val layoutImage = ImageView(this)
                Glide.with(this)
                        .asBitmap()
                        .load(File("$basePath/${content.path}").toUri())
                        .centerCrop()
                        .dontAnimate()
                        .into(layoutImage)
                return layoutImage
            } else if (content.type == "video") {
                return initVideoPlayer(currentContent)
            }
        }
        return null
    }

    private fun startCountDownContentDuration(layoutPlayer: FrameLayout, layoutParams: RelativeLayout.LayoutParams, selectedPlayer: Playlist, index: Int = 0) {
        val timerContent = object : CountDownTimer(selectedPlayer.contents[index].duration * 1000L, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                println("Content ${selectedPlayer.contents[index].mediaId} duration = " + +millisUntilFinished / 1000)
            }

            override fun onFinish() {
                logHelper("onFinish selectedContent - ${selectedPlayer.contents[index].mediaId}")

                hashMapVideoPlayer[selectedPlayer.contents[index].mediaId]?.release()
                val newIndex = if (index + 1 < selectedPlayer.contents.size) index + 1 else 0
                initPlayer(layoutPlayer, layoutParams, selectedPlayer, newIndex)
            }
        }
        timerContent.start()
        listTimerContent.add(timerContent)
    }

    private fun startCountDownTimelineDuration() {
        val checkCampaign = checkDateTimeCampaign()
        if (checkCampaign != null && checkCampaign != selectedCampaign) initCampaign()

        timerTimeline = object : CountDownTimer(selectedElement.duration * 1000L, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                println("TimelineElement ${selectedElement.id} duration = " + millisUntilFinished / 1000)
            }

            override fun onFinish() {
                logHelper("onFinish selectedElement - ${selectedElement.id}")
                if (currentElementIndex + 1 < allTimelineElement.size)
                    currentElementIndex++
                else
                    currentElementIndex = 0
                selectedElement = allTimelineElement[currentElementIndex]

                initLayoutElement()
                startCountDownTimelineDuration()
            }
        }
        timerTimeline.start()
    }

    private fun initVideoPlayer(currentContent: Content): View? {
        if (!File("$basePath/${currentContent.path}").exists()) return null

        val mediaSource = ProgressiveMediaSource.Factory(mediaDataSourceFactory).createMediaSource(Uri.parse("file:///$basePath/${currentContent.path}"))

        val exoPlayer = ExoPlayerFactory.newSimpleInstance(this)
        with(exoPlayer) {
            prepare(mediaSource)
            repeatMode = if (currentContent.loop) Player.REPEAT_MODE_ONE else Player.REPEAT_MODE_OFF
            playWhenReady = currentContent.autoPlay
            if (currentContent.muted) volume = 0f
            if (currentContent.startTimeOffset != 0) seekTo(currentContent.startTimeOffset.toLong())
        }

        hashMapVideoPlayer[currentContent.mediaId] = exoPlayer
        val playerView = CustomPlayerView(this)
        with(playerView) {
            useController = false
            setBackgroundColor(Color.TRANSPARENT)
            when (currentContent.scale) {
                "fill" -> resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FILL
                "cover" -> resizeMode = AspectRatioFrameLayout.RESIZE_MODE_ZOOM
                "contain" -> resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FIXED_WIDTH
                "scale-down" -> resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FIXED_WIDTH
            }
            isFocusable = true
            setShutterBackgroundColor(Color.TRANSPARENT)
            player = exoPlayer
            requestFocus()
        }
        return playerView
    }

    override fun onStart() {
        super.onStart()
        if (isNetworkConnectedOrConnecting()) {
            FirebaseDatabase.getInstance().getReference(BuildConfig.PREFIX_TYPE)
                    .child(branchName.toLowerCase())
                    .child(directoryName.toLowerCase())
                    .child("is_playing").setValue(1)
        }
    }

    override fun onDestroy() {
        if (::timerTimeline.isInitialized) timerTimeline.cancel()
        listTimerContent.forEach { it.cancel() }
        hashMapVideoPlayer.values.forEach { it.release() }
        super.onDestroy()
    }

    private fun hideSystemUI() {
        window.decorView.systemUiVisibility = (//View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        or View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        or View.SYSTEM_UI_FLAG_IMMERSIVE)
    }

    private fun showSystemUI() {
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
    }

}